import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import { HashRouter as Router } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';

// https://developer.mozilla.org/ru/docs/Web/API/Window/resize_event
(function() {
    var throttle = function(type, name, obj) {
        obj = obj || window;
        
        var timeoutId;
        var func = function() {
            clearTimeout(timeoutId)
            timeoutId = setTimeout(function() {
                obj.dispatchEvent(new CustomEvent(name));
            }, 500);
        };
        obj.addEventListener(type, func);
    };

    throttle("resize", "optimizedResize");
})();

ReactDOM.render(
    <React.StrictMode>
        <Router>
            <App />
        </Router>
    </React.StrictMode>,
    document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
