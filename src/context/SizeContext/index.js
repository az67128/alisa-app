import {createContext} from 'react'
const SizeContext = createContext({height: window.innerHeight, width: window.innerWidth});
export default SizeContext