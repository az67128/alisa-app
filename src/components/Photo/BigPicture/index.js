import Status from '../Status'
import Header from './Header';
import Footer from './Footer';
import styles from '../homepage.module.css';
const BigPicture = ({setBigPic, bigPic}) => {

    return (
        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',

                width: '100%',
                height: '100%',
                position: 'absolute',
                backgroundColor: '#FFF',
            }}
        >
            <div className={styles.attached}>
                <Status />
                <Header close={() => setBigPic(null)} />
            </div>
            <img src={bigPic} alt="preview" style={{ width: '100%' }} />
            <div className={styles.attached}>
                <Footer />
            </div>
        </div>
    );
}

export default BigPicture;
