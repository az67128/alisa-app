import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      viewBox="0 0 1080 223"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g filter="url(#prefix__filter0_b)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M0 0h1080v223H0V0z"
          fill="#F8F8F8"
          fillOpacity={0.92}
        />
      </g>
      <path
        d="M86.673 47H105v54H49V47h19.855"
        stroke="#007AFF"
        strokeWidth={3}
      />
      <path fill="#007AFF" d="M76 28h3v38h-3z" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M87.467 38L89 36.29 77.51 25 66 36.289 67.917 38l9.59-9.559L87.468 38z"
        fill="#007AFF"
      />
      <path
        stroke="#007AFF"
        strokeWidth={3}
        d="M992.288 45.965l.425 45.042M1015.11 45.946l-1.21 45.027M1003.5 46v45M981 37l.902 58.078a5 5 0 004.999 4.922h33.199a5 5 0 005-4.922L1026 37"
      />
      <path fill="#007AFF" d="M976 34h55v3h-55z" />
      <path
        d="M1016 34v-5c0-1.657-1.34-3-3-3h-19a3 3 0 00-3 3v5"
        stroke="#007AFF"
        strokeWidth={3}
      />
      <path
        clipRule="evenodd"
        d="M540.5 95c65.055-43.797 22.186-79.256 0-55.238-22.186-24.018-65.055 11.441 0 55.238z"
        stroke="#007AFF"
        strokeWidth={3}
      />
      <defs>
        <filter
          id="prefix__filter0_b"
          x={-27.183}
          y={-27.183}
          width={1134.37}
          height={277.366}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feGaussianBlur in="BackgroundImage" stdDeviation={13.591} />
          <feComposite
            in2="SourceAlpha"
            operator="in"
            result="effect1_backgroundBlur"
          />
          <feBlend
            in="SourceGraphic"
            in2="effect1_backgroundBlur"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  )
}

export default SvgComponent
