import { useRef, useContext, useEffect, useState } from 'react';
import { list } from 'constansts/photo'; //basic array
import SizeContext from 'context/SizeContext';
import style from './flow.module.css';

export function useOnScreen(ref, callback, rootMargin = '0px') {
    useEffect(() => {
        const storedRef = ref.current; // костыль?
        const observer = new IntersectionObserver(
            ([entry]) => {
                if (entry.isIntersecting) callback();
            },
            {
                rootMargin,
            },
        );
        if (ref.current) {
            observer.observe(ref.current);
        }
        return () => {
            observer.unobserve(storedRef);
        };
    }, [ref.current]);
}

const PhotoFlow = ({ setBigPic }) => {
    const [photosCount, setPhotosCount] = useState(28);
    const photos = list.slice(0, photosCount);
    const bottomRef = useRef();
    const photoUrl = (filename, fullsize = false) =>
        `${process.env.PUBLIC_URL}/photos/${fullsize ? 'big' : 'preview'}/${filename}`;
    useOnScreen(bottomRef, ()=>setPhotosCount(state=>state+ 8));

    const { width } = useContext(SizeContext);

    return (
        <div className={style.mosaic}>
            {photos.map((item) => (
                <img
                    key={item}
                    src={photoUrl(item)}
                    alt={item}
                    onClick={(e) => {setBigPic(photoUrl(item, true)); e.stopPropagation();}}
                    style={{ 
                        width: 0.24*width,
                        height: 0.24*width,
                        margin: 0.005*width,
                    }}
                />
            ))}
            <div ref={bottomRef} />
        </div>
    );
};
export default PhotoFlow;
