import React, {useState} from 'react';
import Header from './Header';
import Footer from './Footer';
import PhotoFlow from './PhotoFlow';
import Status from './Status';
import BigPicture from './BigPicture';
import styles from './homepage.module.css';

const Photo = () => {
    const [bigPic, setBigPic] = useState(null);
    return (
        <div className={styles.app}>
            {bigPic && <BigPicture bigPic={bigPic} setBigPic={setBigPic} />}
            <div className={styles.attached}>
                <Status />
                <Header />
            </div>
            <div className={styles.body}>
                <PhotoFlow setBigPic={setBigPic}/>
            </div>
            <div className={styles.attached}>
                <Footer />
            </div>
        </div>
    );
};

export default Photo;
