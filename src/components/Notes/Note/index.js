import {useContext} from 'react'
import Header from './Header';
import notes from 'constansts/notes';
import { useParams } from 'react-router-dom';
import SizeContext from 'context/SizeContext';
const Note = () => {
    const { height } = useContext(SizeContext);
    const { id } = useParams();
    const note = notes[id];
    return (
        <>
            <div>
                <Header />
            </div>
            <div style={{ whiteSpace:'pre-line', fontSize: height * 0.02, padding: height * 0.02 }}>{note.text}</div>
        </>
    );
};

export default Note;
