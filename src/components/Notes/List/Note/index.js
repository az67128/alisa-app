import { useContext } from 'react';
import { Link } from 'react-router-dom';
import styles from './note.module.css';
import SizeContext from 'context/SizeContext';
const Note = ({ note: { title, date, description }, i }) => {
    const { height } = useContext(SizeContext);
    return (
        <Link to={`/notes/${i}`}>
            <div style={{ paddingLeft: height * 0.039, paddingTop: height * 0.01 }} className={styles.container}>
                {i === 0 && <div className={styles.divider} style={{ marginBottom: height * 0.01 }}></div>}
                <div className={styles.title} style={{ fontSize: height * 0.024, marginBottom: height * 0.01 }}>
                    {title}
                </div>
                <div className={styles.description} style={{ fontSize: height * 0.02 }}>
                    <span className={styles.date}>{date}</span>&nbsp;
                    <span className={styles.description}>{description}</span>
                </div>
                <div className={styles.divider} style={{ paddingBottom: height * 0.01 }}></div>
                <div className={styles.highlight} data-highlight/>
            </div>
        </Link>
    );
};

export default Note;
