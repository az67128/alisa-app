import Header from './Header';
import Footer from './Footer';
import Note from './Note';
import notes from 'constansts/notes';
import styles from './list.module.css';
const List = () => {
    return (
        <>
            <div>
                <Header />
            </div>
            <div className={styles.content}>
                {notes.map((note, i) => (
                    <Note note={note} i={i}/>
                ))}
            </div>
            <div>
                <Footer />
            </div>
            <div>
                <svg viewBox="0 0 1080 80" />
            </div>
        </>
    );
};

export default List;
