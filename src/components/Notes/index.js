import React from 'react';
import { Route, Switch } from 'react-router-dom';
import List from './List';
import Note from './Note';
import styles from './notes.module.css'
const Notes = () => {
    return (
        <div className={styles.root}>
            <Switch>
                <Route path="/notes/:id" component={Note} />
                <Route path="/notes" exact component={List} />
                
            </Switch>
        </div>
    );
};

export default Notes;
