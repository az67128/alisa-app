import {useState} from 'react'
import StatusBar from './StatusBar';
import Apps from './Apps';
import BottomBar from './BottomBar';
import Efficiency from './Efficiency'
import styles from './desktop.module.css';
const Desktop = () => {
    const [isEfficiencyOpen, setIsEfficiencyOpen] = useState(false)
    return (
        <div className={styles.root}>
            <StatusBar className={styles.statusBar} />
            <div className={styles.appsContainer}>
                <Apps className={styles.apps} setIsEfficiencyOpen={setIsEfficiencyOpen}/>
            </div>
            <BottomBar className={styles.bottomBar}/>
            {isEfficiencyOpen && <div className={styles.efficiency}><Efficiency setIsEfficiencyOpen={setIsEfficiencyOpen}/></div>}
        </div>
    );
};

export default Desktop;
