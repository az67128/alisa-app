import { useRef, useState, useEffect } from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import WhatsApp from '../WhatsApp';
import Desktop from 'components/Desktop';
import Vk from 'components/Vk';
import Notes from 'components/Notes';
import Photo from 'components/Photo';
import styles from './style.module.css';
import Swiper from './Swiper';
import SizeContext from 'context/SizeContext';
import { useSpring, animated } from 'react-spring';

function App() {
    const location = useLocation();
    console.log(location);
    const [{ xys }, set] = useSpring(() => ({ xys: [0, 0, 1, 1] }));
    const appRef = useRef();
    const [size, setSize] = useState({ height: window.innerHeight, width: window.innerWidth });
    useEffect(() => {
        const { width, height } = appRef.current.getBoundingClientRect();
        setSize({ width, height });
    }, [appRef]);

    function makeResize() {
        const { width, height } = appRef.current.getBoundingClientRect();
        setSize({ width, height });
    }
    function highlightActiveAreas(event) {
        if (event.target.dataset.highlight) return;
        if (event.target.dataset.highlightSkip) return;
        document.querySelectorAll('[data-highlight]').forEach((item) => {
            item.style.opacity = 0.7;
            setTimeout(() => (item.style.opacity = 0), 200);
        });
    }
    useEffect(() => {
        window.addEventListener('optimizedResize', makeResize);
        window.addEventListener('click', highlightActiveAreas);
        return () => {
            window.removeEventListener('optimizedResize', makeResize);
            window.removeEventListener('click', highlightActiveAreas);
        };
    });

    return (
        <SizeContext.Provider value={size}>
            <div className={styles.root}>
                <div className={styles.app} ref={appRef}>
                    <div className={styles.appBg} />
                    <animated.div
                        className={styles.appContainer}
                        style={{
                            transform: xys.interpolate((x, y, s) =>
                                location.pathname === '/' ? `` : `translate3d(${x}px, ${y}px, 0) scale(${s})`,
                            ),
                            opacity: xys.interpolate((x, y, s, o) => o),
                        }}
                    >
                        <Switch>
                            <Route path="/whatsapp">
                                <WhatsApp />
                            </Route>
                            <Route path="/vk">
                                <Vk />
                            </Route>
                            <Route path="/photo" exact>
                                <Photo />
                            </Route>
                            <Route path="/notes">
                                <Notes />
                            </Route>
                            <Route path="/" exact>
                                <Desktop />
                            </Route>
                        </Switch>
                        <Swiper xys={xys} set={set} />
                    </animated.div>
                </div>
            </div>
        </SizeContext.Provider>
    );
}

export default App;
