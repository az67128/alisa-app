import React, {  useContext } from 'react';
import styles from './swiper.module.css';
import SizeContext from 'context/SizeContext';
import { useHistory, useLocation } from 'react-router-dom';
import {  animated } from 'react-spring';
import { useDrag } from 'react-use-gesture';

function Swiper({ xys, set }) {
    const { height } = useContext(SizeContext);
    const history = useHistory();
    

    const bind = useDrag(({ down, movement }) => {
        
        set({ xys: down ? [...movement, 1+movement[1]/height, 1] : [0, 0, 1, 1] });
        if(-movement[1]/height > 0.10 && !down ){
          set({ xys: [0, -height*0.5, 0, 0] });
          setTimeout(()=>{
            set({ xys: [0, 0, 1, 1] });
              history.push("/")
          }, 150)
        }
    });

    const location = useLocation();
    if (location.pathname === '/') return null;
    return (
        <animated.div
            className={styles.root}
            {...bind()}
            style={{
                transform:
                    xys.interpolate((x, y, s) => `translate3d(${x}px, ${y*0.1}px, 0)`),

                touchAction: 'pan-x',
            }}
        >
            <svg className={styles.svg} viewBox="0 0 1080 47" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x={348} y={15} width={382} height={11} rx={5.5} fill="#010101" />
            </svg>
        </animated.div>
    );
}

export default Swiper;
