import { Switch, Route } from 'react-router-dom';
import Profile from './Profile';
import Messages from './Messages'
import Navbar from './Navbar'
import Chat from './Chat'
import styles from './vk.module.css';

 <Route path="/vk" exact>
                    <Profile />
                </Route>
const Vk = () => {
    return (
        <div className={styles.root}>
            <Switch>
                <Route path="/vk" exact>
                    <Profile />
                </Route>
                <Route path="/vk/messages" exact>
                    <Messages />
                </Route>
                <Route path="/vk/messages/:id" >
                    <Chat />
                </Route>
            </Switch>
            <div><Navbar className={styles.navBar} /></div>
        </div>
    );
};
export default Vk;
