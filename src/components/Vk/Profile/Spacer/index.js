import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
     
      viewBox="0 0 1080 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path fill="#EBEDF0" d="M0 0h1080v32H0z" />
    </svg>
  )
}

export default SvgComponent
