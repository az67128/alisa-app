import style from './post.module.css';
import { useContext } from 'react';
import SizeContext from 'context/SizeContext';
const Post = ({ post }) => {
    const { height } = useContext(SizeContext);
    const contentPadding = height * 0.019;
    const iconSize = height * 0.03;
    return (
        <div className={style.post}>
            <div className={style.header} style={{ padding: height * 0.024 }}>
                <div
                    className={style.avatar}
                    style={{
                        backgroundImage: `url(${process.env.PUBLIC_URL}${post.avatar})`,
                        width: height * 0.067,
                        height: height * 0.067,
                    }}
                />
                <div style={{ marginLeft: height * 0.0135 }}>
                    <div className={style.name} style={{ fontSize: height * 0.025, marginBottom: height * 0.005 }}>
                        {post.name}
                    </div>
                    <div className={style.date} style={{ fontSize: height * 0.021 }}>
                        {post.date}
                    </div>
                </div>
            </div>
            <div
                className={style.content}
                style={{
                    paddingLeft: contentPadding,
                    paddingRight: contentPadding,
                    fontSize: height * 0.021,
                }}
            >
                {post.content}
                {post.image && (
                    <img alt="post" src={`${process.env.PUBLIC_URL}${post.image}`} className={style.postImage} style={{marginTop: contentPadding}}/>
                )}
            </div>
            <div className={style.bottomBar} style={{padding: contentPadding}}>
              <div className={style.like} style={{width: iconSize, height: iconSize}}/>
              <div className={style.number}>5</div>
              <div style={{width: iconSize}}/>
              <div className={style.comment} style={{width: iconSize, height: iconSize}}/>
              <div className={style.number}>6</div>
              <div style={{width: iconSize}}/>
              <div className={style.share} style={{width: iconSize, height: iconSize}}/>
              <div className={style.number}>2</div>
            </div>
        </div>
    );
};
export default Post;
