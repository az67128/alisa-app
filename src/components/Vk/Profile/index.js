import { Fragment } from 'react';
import Header from './Header';
import ProfileData from './ProfileData';
import Photos from './Photos';
import Spacer from './Spacer';
import WhatsNew from './WhatsNew';
import vkProfiles from 'constansts/vkProfiles';
import LoadMore from './LoadMore'
import Post from './Post';
import styles from './profile.module.css';
const Profile = () => {
    const profile = vkProfiles.misha;
    return (
        <>
            <div>
                <Header className={styles.header} />
            </div>
            <div className={styles.content}>
                <div>
                    <ProfileData profile={profile} />
                </div>
                <div>
                    <Photos className={styles.photos} profile={profile} />
                </div>
                <div>
                    <Spacer className={styles.spcaer} />
                </div>
                <div>
                    <WhatsNew className={styles.whatsNew} profile={profile} />
                </div>
                <div>
                    <Spacer className={styles.spcaer} />
                </div>
                {profile.posts.map((post) => (
                    <Fragment>
                        <Post post={post} />
                        <div>
                            <Spacer className={styles.spcaer} />
                        </div>
                    </Fragment>
                ))}
                {profile.profileUrl &&<div> <LoadMore profileUrl={profile.profileUrl}/> <Spacer className={styles.spcaer} /></div>}
            </div>
        </>
    );
};

export default Profile;
