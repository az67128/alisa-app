
import { useParams } from 'react-router-dom';
import TopBar from './TopBar';
import Message from './Message';
import vkChats from 'constansts/vkChats'

import styles from './chat.module.css';
const Chat = () => {
    const params = useParams();
    
    const messageList = vkChats[params.id].chat
    return (
        <>
            <div>
                <TopBar name={params.id} />
            </div>
            <div className={styles.conversation}>
                {messageList.map((message)=>{
                     return <Message message={message}/>
                })}
                
            </div>
        </>
    );
};
export default Chat;
