import { useContext } from 'react';
import SizeContext from 'context/SizeContext';
import {sendMetrik} from 'helpers'
import style from './message.module.css';

const getYoutubeUrl = (url) => {
    let iframeUrl = url;
    if (!/^http/.test(url)) iframeUrl = 'https://' + url;
    iframeUrl = url.replace('watch?v=', '');
    if (!/^\/\/.+\/embed\//.test(iframeUrl)) {
        iframeUrl = iframeUrl.replace(iframeUrl.match(/\/\/.+\//)[0], '//www.youtube.com/embed/').replace('&', '?&');
    }
   
    return iframeUrl;
};

const Message = ({ message: { sender, text, time, date, attach, sticker } }) => {
    const { height } = useContext(SizeContext);
    const verticalPadding = height * 0.0086;
    const horizontalPadding = height * 0.014;
    const onItemClick = (attach)=>{
        if(attach && attach.startsWith('https://youtu')){
            sendMetrik('ClickVkDialogSeleznevaVideo', {ClickYouTubeVideo:attach})
        }
    }
    if (date) {
        return (
            <div className={`${style.message} ${style.date}`}>
                <div style={{ fontSize: height * 0.02 }}>{date}</div>
            </div>
        );
    }

    return (
        <div className={`${style.message} ${sender === 'я' ? style.myMessage : ''}`}>
            {sticker ? (
                <img
                    style={{ maxHeight: height * 0.25 }}
                    src={`${process.env.PUBLIC_URL}/vk/stickers/${sticker}`}
                    alt="sticker"
                    className={style.sticker}
                />
            ) : (
                <div
                onClick={()=>onItemClick(attach)}
                    className={style.container}
                    style={{
                        borderRadius: height * 0.022,
                        paddingTop: verticalPadding,
                        paddingBottom: verticalPadding,
                        paddingLeft: horizontalPadding,
                        paddingRight: horizontalPadding,
                    }}
                >
                    <span
                        className={style.text}
                        dangerouslySetInnerHTML={{ __html: text }}
                        style={{ fontSize: height * 0.022 }}
                    />
                    {attach && attach.startsWith('https://youtu') && (
                        <div className={style.youtubeWrapper}>
                            <iframe
                                title="youtube player"
                                className={style.youtube}
                                src={getYoutubeUrl(attach)}
                                frameBorder="0"
                                allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                            ></iframe>
                        </div>
                    )}
                    <span className={style.time} style={{ fontSize: height * 0.015, paddingTop: height * 0.012 }}>
                        {time}
                    </span>
                    <div className={style.clear} style={{ fontSize: height * 0.01 }} />
                </div>
            )}
        </div>
    );
};
export default Message;
