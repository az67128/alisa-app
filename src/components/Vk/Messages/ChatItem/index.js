import * as React from 'react';
import './index.css';
import { Link } from 'react-router-dom';
import {sendMetrik} from 'helpers'
function SvgComponent({ item }) {
    
    if (!item) return null;
    const { from, message, time, name, avatar } = item;
    const onClick = (e)=>{
        e.stopPropagation()
        sendMetrik('ClickVkDialogOther', {ClickVkDialogName:name})
    }
    return (
        <Link to={`/vk/messages/${name}`} onClick={onClick}>
            <svg viewBox="0 0 1080 188" fill="none" xmlns="http://www.w3.org/2000/svg">
                <foreignObject x={41} y={20} width={148} height={148}>
                    <div className="vk_chatItem_avatar" style={{backgroundImage: `url('${process.env.PUBLIC_URL}${avatar}')`}}></div>
                </foreignObject>
                {/* <circle cx={115} cy={94} r={74} fill="#C4C4C4" className="vk_chatItem_avatar"/> */}
                <foreignObject x={213} y={35} width={827} height={52}>
                    <div className="vk_chatItem_title">{name}</div>
                </foreignObject>
                <foreignObject x={213} y={97} width={827} height={52}>
                    <div className="vk_chatItem_message">
                        {from && <div className="vk_chatItem_messageFrom">{from}:&nbsp;</div>}
                        <div className="vk_chatItem_messageText">{message}</div>
                        <div className="vk_chatItem_messageTime">· {time}</div>
                    </div>
                </foreignObject>
                <circle cx={115} cy={94} r={74}  data-highlight/>
            </svg>
        </Link>
    );
}

export default SvgComponent;
