import TopBar from './TopBar';
import ChatItem from './ChatItem';
import Search from './Search';
import styles from './messages.module.css';
import vkChats from 'constansts/vkChats';
const Messages = () => {
    console.log(vkChats);
    return (
        <>
            <div>
                <TopBar />
            </div>
            
                <div class={styles.chatList}>
                    <Search />
                    {Object.keys(vkChats).map((key) => (
                        <div key={key}>
                            <ChatItem item={vkChats[key]} />
                        </div>
                    ))}
                </div>
            
        </>
    );
};

export default Messages;
