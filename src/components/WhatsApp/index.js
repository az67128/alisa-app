import React from 'react'
import { Route, Switch } from 'react-router-dom';
import Home from './Home';
import Chat from './Chat';

const WhatsApp = () => {
    return (
        <Switch>
            <Route path="/whatsapp" exact component={Home}/>
            <Route path="/whatsapp/:id" component={Chat} />
        </Switch>
    )
}

export default WhatsApp;