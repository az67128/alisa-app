import style from './ChatItem.module.css';
import {chats} from 'constansts/whatsapp'
import photo from 'images/WhatsApp/ChatAvatar/default.png';

function SvgComponent(props) {
    const chat = chats[props.title];
    const lastMessage = chat.messages[chat.messages.length - 1];
    const avatarName = chat.avatar;
    return (
        <svg
            viewBox="0 0 375 74"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            {...props}
        >
            <foreignObject x="0" y="0" width="74" height="74" >
                <div className={style.avatar}>
                    <img src={avatarName ? `${process.env.PUBLIC_URL}/whatsapp/avatars/${avatarName}` : photo} alt="avatar"  className={style.image}/>
                </div>
            </foreignObject>
            
            <circle cx={38} cy={38} r={28}  data-highlight/>

            <foreignObject x="80" y="8" width="200" height="21">
                <div className={style.title}>
                    {props.title}
                </div>
            </foreignObject>
           
            <foreignObject x="80" y="28" width="260" height="37">
                <div className={style.message}>
                    {lastMessage.text}
                </div>
            </foreignObject>
          
            <path
                d="M354.586 37l-4.293 4.293a1 1 0 101.414 1.414l5-5a1 1 0 000-1.414l-5-5a1 1 0 10-1.414 1.414L354.586 37z"
                fill="#3C3C43"
                fillOpacity={0.3}
            />
            <path xmlns="http://www.w3.org/2000/svg" d="M80 73 H 375" stroke="#3C3C43" strokeOpacity="0.29" strokeWidth="0.33" strokeLinecap="square"/>
        </svg>
    );
}

export default SvgComponent;
