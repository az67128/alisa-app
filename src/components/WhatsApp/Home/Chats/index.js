import React, { Fragment } from 'react';
import {Link} from 'react-router-dom'
import ChatItem from './ChatItem'
import {chats} from 'constansts/whatsapp'
import {sendMetrik} from 'helpers'

const Chats = () => {
    const onClick =(e, key)=>{
        e.stopPropagation()
        sendMetrik('ClickWhatsappDialog', {ClickWhatsappDialogName:key})
    }
return(
    <Fragment>
        {Object.keys(chats).map((key) => (
            <Link to={`whatsapp/${key}`}  key={key} onClick={(e)=>onClick(e, key)}>
                <ChatItem key={key} title={key} />
            </Link>
            )    
        )}
    </Fragment>
)
};

export default Chats;
