import React from 'react';
import Header from './Header';
import Nav from './Nav';
import Chats from './Chats';

import styles from './homepage.module.css';

const WhatsApp = () => {
    return (
        <div className={styles.app}>
            <div className={styles.attached}>
                <Header />
            </div>
            <div className={styles.body}>
                <Chats />
            </div>
            <div className={styles.attached}>
                <Nav />
            </div>
        </div>
    );
};

export default WhatsApp;
