import React from 'react';
import Header from './Header';
import Bottom from './Bottom';
import Message from './Message';
import { chats } from 'constansts/whatsapp';
import styles from './chat.module.css';

const Chat = ({ match }) => {
    const chat = chats[match.params.id];
    return (
        <div 
            className={styles.app}                
            style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/whatsapp/chatBackground.svg)` }}
        >
            <div className={styles.attached}>
                <Header name={match.params.id} />
            </div>

            <div className={styles.body}>
                {chat.messages.map((message) => (
                    <Message message={message} key={message.text} chatType={chat.type} />
                ))}
            </div>

            <div className={styles.attached}>
                <Bottom />
            </div>
        </div>
    );
};

export default Chat;
