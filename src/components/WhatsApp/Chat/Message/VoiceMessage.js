import React, { Fragment, useState, useEffect } from 'react';
import styles from '../chat.module.css';

export const useAudio = (url) => {
    const [audio] = useState(new Audio(url));
    const [playing, setPlaying] = useState(false);
    const [duration, setDuration] = useState(0);
    const [currentTime, setCurrentTime] = useState(0);

    const toggle = () => setPlaying(!playing);

    useEffect(() => {
        playing ? audio.play() : audio.pause();
    }, [playing, audio]);

    useEffect(() => {
        audio.addEventListener('ended', () => setPlaying(false));
        audio.addEventListener('durationchange', () =>
            setDuration(audio.duration)
        );
        audio.addEventListener('timeupdate', () =>
            setCurrentTime(audio.currentTime)
        );
        return () => {
            audio.removeEventListener('ended', () => setPlaying(false));
            audio.removeEventListener('durationchange', () =>
                setDuration(audio.duration)
            );
            audio.removeEventListener('timeupdate', () =>
                setCurrentTime(audio.currentTime)
            );
        };
    }, [audio]);

    return [playing, toggle, duration, currentTime];
};

function SvgComponent(props) {
    const { filename, avatar } = props;
    const [isPlaying, togglePlay, duration, currentTime] = useAudio(
        `${process.env.PUBLIC_URL}/whatsapp/audio/${filename}`
    );

    const time = (sec) => {
        const seconds = ('00' + Math.floor(sec % 60)).slice(-2);
        const minutes = Math.floor(sec / 60);
        return `${minutes}:${seconds}`;
    };

    const persentsPlayed =
        duration && Math.floor((currentTime / duration) * 100);

    return (
        <svg
            viewBox="0 0 869 170"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#prefix__filter0_d)">
                <rect x={1} width={867} height={168} rx={24} fill="#FCFBFB" />
            </g>
            <foreignObject x={698} y={9} width={144} height={144}>
                <div className={styles.avatar} style={{ padding: 0 }}>
                    <img
                        src={`${process.env.PUBLIC_URL}/whatsapp/avatars/${avatar}`}
                        alt="avatar"
                        className={styles.image}
                    />
                </div>
            </foreignObject>

            {isPlaying ? (
                <Fragment>
                    <rect x={62} y={50} width="16" height="51" fill="#80847D" />
                    <rect x={85} y={50} width="16" height="51" fill="#80847D" />
                </Fragment>
            ) : (
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M105 76.5L60 102V51l45 25.5z"
                    fill="#80847D"
                />
            )}
            <path fill="#E2E2E2" d="M156 69h507v15H156z" />

            <rect
                x={155 + 5 * persentsPlayed}
                y={54}
                width={9}
                height={45}
                rx={1.5}
                fill="#007AFF"
            />

            <foreignObject x={152} y={105} width={100} height={42}>
                <div style={{ fontSize: '36px', color: '#848484' }}>
                    {isPlaying ? time(currentTime) : time(duration)}
                </div>
            </foreignObject>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M720 96a9 9 0 019 9v21a9 9 0 01-9 9 9 9 0 01-9-9v-21a9 9 0 019-9zm15.75 19.5a2.25 2.25 0 00-2.25 2.25V126c0 7.456-6.044 13.5-13.5 13.5s-13.5-6.044-13.5-13.5v-8.25a2.25 2.25 0 10-4.5 0V126c.004 9.068 6.753 16.717 15.75 17.85v4.65h-6a2.25 2.25 0 100 4.5h16.5a2.25 2.25 0 100-4.5h-6v-4.65c8.997-1.133 15.746-8.782 15.75-17.85v-8.25a2.25 2.25 0 00-2.25-2.25z"
                fill="#007AFF"
            />
            <path
                d="M733.5 126h.25-.25zm-27-8.25h.25-.25zM702 126h-.25.25zm15.75 17.85h.25v-.22l-.219-.028-.031.248zm0 4.65v.25h.25v-.25h-.25zm-6 0v.25-.25zm10.5 0H722v.25h.25v-.25zm0-4.65l-.031-.248-.219.028v.22h.25zM738 126h.25-.25zm-18-29.75a8.75 8.75 0 018.75 8.75h.5a9.25 9.25 0 00-9.25-9.25v.5zm8.75 8.75v21h.5v-21h-.5zm0 21a8.75 8.75 0 01-8.75 8.75v.5a9.25 9.25 0 009.25-9.25h-.5zm-8.75 8.75a8.75 8.75 0 01-8.75-8.75h-.5a9.25 9.25 0 009.25 9.25v-.5zm-8.75-8.75v-21h-.5v21h.5zm0-21a8.75 8.75 0 018.75-8.75v-.5a9.25 9.25 0 00-9.25 9.25h.5zm24.5 10.25a2.5 2.5 0 00-2.5 2.5h.5a2 2 0 012-2v-.5zm-2.5 2.5V126h.5v-8.25h-.5zm0 8.25c0 7.318-5.932 13.25-13.25 13.25v.5c7.594 0 13.75-6.156 13.75-13.75h-.5zM720 139.25c-7.318 0-13.25-5.932-13.25-13.25h-.5c0 7.594 6.156 13.75 13.75 13.75v-.5zM706.75 126v-8.25h-.5V126h.5zm0-8.25a2.5 2.5 0 00-2.5-2.5v.5a2 2 0 012 2h.5zm-2.5-2.5a2.5 2.5 0 00-2.5 2.5h.5a2 2 0 012-2v-.5zm-2.5 2.5V126h.5v-8.25h-.5zm0 8.25c.005 9.194 6.847 16.949 15.969 18.098l.062-.496c-8.871-1.118-15.527-8.66-15.531-17.602h-.5zm15.75 17.85v4.65h.5v-4.65h-.5zm.25 4.4h-6v.5h6v-.5zm-6 0a2.5 2.5 0 00-2.5 2.5h.5a2 2 0 012-2v-.5zm-2.5 2.5a2.5 2.5 0 002.5 2.5v-.5a2 2 0 01-2-2h-.5zm2.5 2.5h16.5v-.5h-16.5v.5zm16.5 0a2.5 2.5 0 002.5-2.5h-.5a2 2 0 01-2 2v.5zm2.5-2.5a2.5 2.5 0 00-2.5-2.5v.5a2 2 0 012 2h.5zm-2.5-2.5h-6v.5h6v-.5zm-5.75.25v-4.65h-.5v4.65h.5zm-.219-4.402c9.122-1.149 15.964-8.904 15.969-18.098h-.5c-.004 8.942-6.66 16.484-15.531 17.602l.062.496zM738.25 126v-8.25h-.5V126h.5zm0-8.25a2.5 2.5 0 00-2.5-2.5v.5a2 2 0 012 2h.5z"
                fill="#fff"
            />

            <circle
                cx={70}
                cy={80}
                r={40}
                fill="#5C80B4"
                opacity={0}
                onClick={(e) => {togglePlay(); e.stopPropagation();}}
                data-highlight
            />

            <defs>
                <filter
                    id="prefix__filter0_d"
                    x={0}
                    y={0}
                    width={869}
                    height={170}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    />
                    <feOffset dy={1} />
                    <feGaussianBlur stdDeviation={0.5} />
                    <feColorMatrix values="0 0 0 0 0.815051 0 0 0 0 0.795273 0 0 0 0 0.767584 0 0 0 1 0" />
                    <feBlend
                        in2="BackgroundImageFix"
                        result="effect1_dropShadow"
                    />
                    <feBlend
                        in="SourceGraphic"
                        in2="effect1_dropShadow"
                        result="shape"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default SvgComponent;
