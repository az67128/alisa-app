import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      style={{position: 'absolute', bottom: props.disposition}}
      viewBox="0 0 1080 89"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M1039.38 68.939c6.87 1.88 5.76 2.643-1.26 6.19-4.42 2.226-20.86 4.278-20.86 4.278v-32.2s5.68 17.235 22.12 21.732z"
          fill="#DCF8C6"
        />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M987.124 48.656l-16.291 16.562-9.526-5.943-1.307 1.36 10.833 10.989 17.921-21.664-1.63-1.304z"
        fill="#007AFF"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M979.459 65.218l16.291-16.562 1.631 1.304-17.922 21.664-5.66-5.742h.578v-3.834l5.082 3.17zm-10.833-4.583z"
        fill="#007AFF"
      />
    </svg>
  )
}

export default SvgComponent

