import { useContext } from 'react';
import {contacts} from 'constansts/whatsapp';

import Tail from './Tail';
import VoiceMessage from './VoiceMessage'

import SizeContext from 'context/SizeContext';
import styles from './message.module.css';

const Message = ({message, chatType}) => {
    const { height } = useContext(SizeContext);
    const isChatGroup = chatType === 'group';
    const isMessageIncoming = message.sender !== 'me';
    const verticalPadding = height * 0.007;
    const horizontalPadding = height * 0.008;
    const verticalMargin = height * 0.006;
    const horizontalMargin = height * 0.028;
    return (
        <div className={styles.wrapper}>
            <div //контейнер сообщения внешний
                className={isMessageIncoming ? styles.incoming : styles.my}
                style={{
                    paddingTop: verticalMargin,
                    paddingBottom: verticalMargin,
                    paddingLeft: horizontalMargin,
                    paddingRight: horizontalMargin,
                }}
            >   
                {(message.attach === 'voice') && (
                    <div className={styles.voice}>
                        <VoiceMessage  filename={message.audio} avatar={contacts[message.sender].avatar} />
                    </div>
                )}

                {(message.text||(message.attach === 'album')||(message.attach === 'sticker')) && <div //контейнер внутренний - с границами и заливкой
                    className={styles.box}
                    style={{
                        borderRadius: height * 0.01,
                        paddingTop: verticalPadding,
                        paddingBottom: verticalPadding,
                        paddingLeft: horizontalPadding,
                        paddingRight: isMessageIncoming ? horizontalPadding : horizontalPadding*4,
                        fontSize: height * 0.02,
                        maxWidth: '75%',
                    }}
                >
                    {isMessageIncoming && isChatGroup //заголовок - имя отправителя
                    ? <div className={styles.sender}>
                        <div style={{color: contacts[message.sender]?.color || 'black'}}>{message.sender}</div>
                        {contacts[message.sender]?.displayedName 
                            && <div className={styles.contact}>~{contacts[message.sender].displayedName}</div>}
                    </div> 
                    : null}

                    {message.text &&<div>{message.text}</div>}

                    {message.attach && 
                        <div className={styles[message.attach]}>
                            {(message.attach === 'sticker') && <img src={`${process.env.PUBLIC_URL}/whatsapp/stickers/${message.image}`} alt={message.attach} className={styles.img}/>}
                            {(message.attach === 'album') && message.images.map(image => <img src={`${process.env.PUBLIC_URL}/whatsapp/album/${image}`} alt={message.attach} className={styles.img}/>)}
                        </div>
                    }
                </div>}
            </div> 
            {!isMessageIncoming && <Tail disposition={height * 0.008} />}
        </div>
    )
}
export default Message;