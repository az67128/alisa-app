import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      viewBox="0 0 375 81"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g filter="url(#prefix__filter0_d)">
        <path fill="#F6F6F6" d="M0 1h375v80H0z" />
      </g>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M309.293 14.55a2.572 2.572 0 00-1.734-.675h-3.506c-.641 0-1.258.24-1.733.674l-.689.628a2.527 2.527 0 01-1.705.664c-2.858 0-5.176 2.346-5.176 5.24v6.553c0 2.894 2.318 5.241 5.176 5.241h11.648c2.858 0 5.176-2.347 5.176-5.241v-6.552c0-2.895-2.318-5.242-5.176-5.242l-.186-.007a2.24 2.24 0 01-1.325-.58l-.77-.704zm-5.24.525h3.506c.341 0 .67.128.924.36l.771.704a3.441 3.441 0 002.035.89l.235.01a4.021 4.021 0 014.026 4.043v6.552c0 2.235-1.784 4.041-3.976 4.041h-11.648c-2.192 0-3.976-1.806-3.976-4.041v-6.552c0-2.168 1.677-3.932 3.778-4.037l.43-.012a3.729 3.729 0 002.282-.97l.689-.628c.254-.232.583-.36.924-.36zm1.697 3.805a5 5 0 110 10 5 5 0 010-10zm-3.8 5a3.8 3.8 0 117.6 0 3.8 3.8 0 01-7.6 0z"
        fill="#007AFF"
      />
      <rect
        opacity={0.45}
        x={47.25}
        y={8.25}
        width={227.5}
        height={31.5}
        rx={15.75}
        fill="#fff"
        stroke="#8E8E93"
        strokeWidth={0.5}
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M261.5 15.5a6 6 0 016 6c0 1.012-.074 2.097-.221 3.255-.433 2.148-1.212 3.751-2.778 5.317-1.469 1.468-3.084 2.378-5.315 3.102-1.227.366-3.686.326-3.686.326a6 6 0 01-6-6v-6a6 6 0 016-6h6zm0 1.2h-6a4.8 4.8 0 00-4.8 4.8v6a4.8 4.8 0 004.8 4.8l1.86.002c.76-.043 1.14-.398 1.14-1.066v-1.643a5 5 0 014.972-5l.658-.01.413-.012c.377-.016.648-.04.815-.071.592-.113.907-.567.944-1.364L266.3 21.5a4.8 4.8 0 00-4.8-4.8zm4.082 8.979c.132-.025.257-.059.384-.103-.476 1.367-1.245 2.579-2.314 3.648-1.095 1.094-2.431 1.965-4.017 2.611.043-.185.065-.385.065-.599v-1.643a3.8 3.8 0 013.779-3.8l.488-.006c.765-.012 1.287-.046 1.615-.108z"
        fill="#007AFF"
      />
      <path
        d="M23.1 14.706l.1-.006a.8.8 0 01.794.7l.006.1v7.7h7.7a.8.8 0 01.794.7l.006.1a.8.8 0 01-.7.794l-.1.006H24v7.7a.8.8 0 01-.7.794l-.1.006a.8.8 0 01-.794-.7l-.006-.1v-7.7h-7.7a.8.8 0 01-.794-.7L13.9 24a.8.8 0 01.7-.794l.1-.006h7.7v-7.7a.8.8 0 01.7-.794l.1-.006-.1.006z"
        fill="#007AFF"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M349 13a4.5 4.5 0 014.5 4.5v7a4.5 4.5 0 01-9 0v-7A4.5 4.5 0 01349 13zm-7.25 10.328a.6.6 0 01.592.503l.008.097v.132c0 3.74 2.98 6.768 6.65 6.768 3.592 0 6.523-2.9 6.646-6.53l.004-.238v-.121a.6.6 0 011.192-.098l.008.098-.004.37c-.123 4.08-3.269 7.392-7.246 7.696V35.5a.6.6 0 01-1.192.097l-.008-.097v-3.495c-3.974-.304-7.119-3.612-7.246-7.689l-.004-.388a.6.6 0 01.6-.6zm3.95-5.828a3.3 3.3 0 116.6 0v7a3.3 3.3 0 11-6.6 0v-7z"
        fill="#007AFF"
      />
      <defs>
        <filter
          id="prefix__filter0_d"
          x={0}
          y={0.67}
          width={375}
          height={80.33}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy={-0.33} />
          <feColorMatrix values="0 0 0 0 0.65098 0 0 0 0 0.65098 0 0 0 0 0.666667 0 0 0 1 0" />
          <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
        </filter>
      </defs>
    </svg>
  )
}

export default SvgComponent
