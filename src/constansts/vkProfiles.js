const profiles = {
    misha: {
        name: 'Миша Дружнов',
        status: 'жду тебя в ТикТоке @misha.druzhnov',
        avatar: '/vk/profiles/misha/ikIMuyV6A9M.jpg',
        photos: [
            '/vk/profiles/misha/uZUBruR5Og4.jpg',
            '/vk/profiles/misha/8qeME7g2c6Y.jpg',
            '/vk/profiles/misha/pQFJfhSuyOc.jpg',
            '/vk/profiles/misha/Ff48eEMruV0.jpg',
        ],
        profileUrl:'https://vk.com/misha_druzhnov',
        photosUrl:'https://vk.com/misha_druzhnov?z=albums554905650',
        posts: [
            {
                avatar: '/vk/profiles/misha/ikIMuyV6A9M.jpg',
                name: 'Миша Дружнов',
                date: '08.12.2020 в 15:35',
                content: `Слушайте, а вы бы приняли на моем месте вызов Вадима? Пишите в комменты, обсудим 👇`,
                image: '/vk/profiles/misha/uZUBruR5Og4.jpg',
            },
            {
                avatar: '/vk/profiles/misha/ikIMuyV6A9M.jpg',
                name: 'Миша Дружнов',
                date: '05.12.2020 в 13:01',
                content: `Кстати, о новой серии!

                3 причины, по которым я ругаюсь с родителями:
                1) не слушаюсь бабушку
                2) конфликтую с одноклассниками
                3) не отвечаю на звонки
                
                А у вас какие топ-3 причины ссор?`,
                image: '/vk/profiles/misha/8qeME7g2c6Y.jpg',
            },
            {
                avatar: '/vk/profiles/misha/ikIMuyV6A9M.jpg',
                name: 'Миша Дружнов',
                date: '04.12.2020 в 14:09',
                content: `Как там раньше говорили? О НАБОЛЕВШЕМ? Математику вот просто терпеть не могу!! Вообще 👻

                Есть предметы в школе, которые ну очень раздражают? Делись в комментариях, посочувствуем друг другу`,
                image: '/vk/profiles/misha/gAVMmhHQy00.jpg',
            },
        ],
    },
};

export default profiles;
