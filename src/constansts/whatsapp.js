﻿export const contacts = {
    '79586378660': {
        displayedName: 'красный',
        color: 'red',
    }, 
    '79296110352': {
        displayedName: 'синий',
        color: 'blue',
    }, 
    '79096834394': {
        displayedName: 'зеленый',
        color: 'green',
    }, 
    '79660637330': {
        displayedName: 'оранжевый',
        color: 'orange',
    }, 
    '79257318102': {
        displayedName: 'пурпурный',
        color: 'purple',
    }, 
    '79096817494': {
        displayedName: 'розовый',
        color: 'pink',
    }, 
    '79169568917': {
        displayedName: 'серый',
        color: 'grey',
    }, 
    '79169570583': {
        displayedName: 'желтый',
        color: 'gold',
    }, 
    '79851155674': {
        displayedName: 'коричневый',
        color: 'brown',
    }, 
    '79169569106': {
        displayedName: 'хаки',
        color: 'darkkhaki',
    },
    '79856740088': {
        displayedName: 'ну очень длинное имя контакта',
        color: 'aquamarine',
    },
    '79856834394': {
        displayedName: 'стальной',
        color: 'steelblue',
    },
    '79779643784': {
        displayedName: 'хтото',
        color: 'darkblue',
    },
    'Староста Инна': {
        inContactList: true,
        color: 'aqua',  
        avatar: 'inna.jpeg',      
    },
    'Вадим Козел': {
        inContactList: true,
        color: 'darkred',
        avatar: 'vadim.png',        
    },
    'Юля': {
        inContactList: true,
        color: 'orange',
        avatar: 'julya.png',       
    },
    'Гриша': {
        inContactList: true,
        color: 'darkgreen',        
    },
    'Коля': {
        inContactList: true,
        color: 'lime',        
    },
    'Макс': {
        inContactList: true,
        color: 'darkred',        
    },
    'Петек': {
        inContactList: true,
        color: 'blue',        
    },
    'Евгения': {
        inContactList: true,
        color: 'green',        
    },
}

export const chats = {
    '5-й БЭЭЭ': {
        type: 'group',
        avatar: '5b.png',
        messages: [
            {
                sender: '79096834394',
                text: 'Все видели видео?',
                time: '(18:42)',
            },
            {
                sender: '79660637330',
                text: 'Хахах норм зарофлили над этим лохом',
            },
            {
                sender: '79257318102',
                text: 'Мамкин гангста бой',
            },
            {
                sender: '79257318102',
                attach: 'sticker',
                image: '1.png',
            },
            {
                sender: '79296110352',
                text: 'Жалкое зрелище',
            },
            {
                sender: '79586378660',
                text: 'НОВИЧЕК жжет ахаха',
            },
            {
                sender: '79169568917',
                text: 'Ахахахахазахаз',
            },
            {
                sender: '79856834394',
                text: 'ЛОЛ',
            },
            {
                sender: '79169570583',
                text: 'Мамкин блоггер!!11',
            },
            {
                sender: '79851155674',
                text: 'Ахахахахахаха лол',
            },
            {
                sender: 'Староста Инна',
                text: 'Ребята, прекратите!',
            },
            {
                sender: '79169568917',
                text: 'ахахахазазхааз',
            },
            {
                sender: '79660637330',
                text: 'Уверен, что видео из-за него заблокировали!',
                time: '(20:40)',
            },
            {
                sender: '79096834394',
                attach: 'sticker',
                image: '2.png',
            },
            {
                sender: '79856740088',
                text: 'Ну мы ему завтра устроим!',
            },
            {
                sender: '79257318102',
                text: 'Московский лошок! Пусть мы ему спуску не дадим! ',
                time: '(20:41)',
                attach: 'sticker',
                image: '3.png',
            },
            {
                sender: '79779643784',
                text: 'Нашел перезалив на крупной платформе! https://screenlifer.com/projects/pranki/',
                attach: 'link',
            },
            {
                sender: '79851155674',
                attach: 'sticker',
                image: '4.png',
            },
            {
                sender: 'Староста Инна',
                text: 'Мы тоже не должны сидеть без дела!',
                time: '(16:24)',
            },
            {
                sender: 'Вадим Козел',
                text: 'Хочешь прогулять школу так и скажи XD',
            },
            {
                sender: 'Юля',
                attach: 'sticker',
                image: '5.gif',
            },
            {
                sender: 'Юля',
                text: 'Можем устроить пикет в поддержку местного питомника',
                time: '(16:24)',
                date: '6 Oct',
            },
            {
                sender: 'Гриша',
                text: 'Да у вас тут отряд боевых ведьм!',
            },
            {
                sender: 'Староста Инна',
                text: 'Хватит ее троллить! Она делает много для нашего будущего!',
            },
            {
                sender: 'Юля',
                text: 'Вадик иди в пень',
            },
            {
                sender: 'Староста Инна',
                text: 'Пойдем на пикет к Цирку – пусть прекратят мучить тигров!',
            },
            {
                sender: 'Вадим Козел',
                text: 'Я пойду чисто на вас-мартышек позырить)))',
            },
            {
                sender: 'Юля',
                text: 'В зеркало позырь, бабуин с камерой!',
            },
            {
                sender: 'Гриша',
                text: 'Жжешь!!! АААА',
            },
            {
                sender: 'Гриша',
                attach: 'sticker',
                image: '6.png',
            },
            {
                sender: 'Вадим Козел',
                text: 'А вы видели мое новое видео на ТикТок?',
            },
            {
                sender: 'Вадим Козел',
                text: 'Новичок, зацени XDDDD',
            },
            {
                sender: 'Вадим Козел',
                text: '(ссылка)',
            },
            {
                sender: 'Староста Инна',
                text: 'Ну мы так не соберемся!',
            },
            {
                sender: 'Юля',
                text: '3 за цирк и 3 за питомник :(',
            },
            {
                sender: 'me',
                text: 'Я за цирк! Пойдемте, там будет веселее!',
            },
            {
                sender: 'Староста Инна',
                text: 'Решено!',
            },

            {
                sender: 'Староста Инна',
                text: 'Билеты будут стоит 500р',
            },
            {
                sender: 'Староста Инна',
                attach: 'album',
                images: ['photo1.jpg','photo2.jpg'],
                time: '(20:43)',
                date: '6 Oct',
            },
            {
                sender: 'Вадим Козел',
                text: 'Ну че, всех животных спасли?',
            },
            {
                sender: 'Вадим Козел',
                attach: 'sticker',
                image: '7.png',
            },
            {
                sender: 'Гриша',
                text: 'XDXDXD',
            },
            {
                sender: 'Староста Инна',
                attach: 'voice',
                audio: 'voice1.mp3',
            },
            {
                sender: 'Вадим Козел',
                attach: 'voice',
                audio: 'voice2.mp3',
            },
            {
                sender: 'Гриша',
                text: 'XDXDXD',
            },
            {
                sender: 'Юля',
                attach: 'voice',
                audio: 'voice3.mp3',
            },
            {
                sender: 'Гриша',
                text: 'Ахахахазхза',
            },
            {
                sender: 'Юля',
                text: 'Это залет ребята! 🔥 Просто оруууу с этого видео',
                time: '(07:29)',
                date: '7 Oct',            
            },
            {
                sender: 'Вадим Козел',
                text: 'Хрррррош пацан, я давно так не угарал. Он вас с вашим пикетом переплюнул на раз-два!',
            },
            {
                sender: 'Коля',
                text: 'Просто оруууууууу',
            },
            {
                sender: 'Гриша',
                text: 'Новичок красавчик! Пацан то оттянулся!',
                time: '(07:30)',
            },
            {
                sender: 'Гриша',
                attach: 'sticker',
                image: '8.png',
            },
            {
                sender: 'Вадим Козел',
                text: 'Фреш пацан! Скоро будет еще!',
            },
            {
                sender: 'Староста Инна',
                text: 'Успокойтесь уже',
            },
            {
                sender: 'Гриша',
                text: 'Хочу еще еще и еще',
            },
        ],
    },
    'Призвание!': {
        type: 'group',
        avatar: 'vocation.png',
        messages: [
            {
                sender: 'Макс',
                text: 'Так, всем привет! Кажется всех собрал',
            },
            {
                sender: 'Макс',
                text: 'Приходим в 11:00 все подготовленные, у нас на разминку времени не будет',
            },
            {
                sender: 'Петек',
                text: 'Ну я опаздаю)))',
            },
            {
                sender: 'Макс',
                text: 'Как всегда.',
            },
            {
                sender: 'Юля',
                text: 'Кто-нибудь может распечатать? Я файл пришлю',
            },
            {
                sender: 'Евгения',
                text: 'Для чего?',
            },
            {
                sender: 'Юля',
                text: 'Для читки',
            },
        ],
    },
    'Староста Инна': {
        type: 'private',
        avatar: 'inna.jpeg',
        messages: [
            {
                sender: 'Староста Инна',
                text: 'Миш, так ты завтра придешь?',
            },
            {
                sender: 'Староста Инна',
                text: 'Миш, Мне звонил твой папа!',
                time: '(23:40)',
            },
        ],
    },
    'Татьяна Васильевна': {
        type: 'private',
        messages: [
            {
                sender: 'Татьяна Васильевна',
                text: 'Михаил, добрый день! Хотела вам сказать, что Амина заболела, поэтому вы будете замените ее в подгруппе! Свяжитесь с Инной!',
            },
            {
                sender: 'me',
                text: 'Хорошо!',
            },
        ],
    },
    'Саня Двор': {
        type: 'private',
        avatar: 'sanya.png',
        messages: [
            {
                sender: 'Саня Двор',
                text: 'Нахабино Затаилось?',
            },
        ],
    },
    'Мама': {
        type: 'private',
        avatar: 'mother.png',
        messages: [
            {
                sender: 'me',
                text: '4279400012342851',
            },
        ],
    },
    'Вадим Козел': {
        type: 'private',
        avatar: 'vadim.png',
        messages: [
            {
                sender: 'ВАДИМ',
                text: 'Эй, сыкун, возьми трубу.',
                time: '(08:18)',
                date: '4 Oct',
            },
            {
                sender: 'ВАДИМ',
                text: 'Слабо, да?',
            },
            {
                sender: 'ВАДИМ',
                text: 'Что, без разрешения мамочки даже телефон не берешь?',
            },
            {
                sender: 'ВАДИМ',
                text: 'Ты куда исчез, пацан?? Включайся!!',
                time: '(07:38)',
                date: '7 Oct',
            },
            {
                sender: 'ВАДИМ',
                text: 'Ты ваще сумасшедший! Давай еще!',
            },
            {
                sender: 'me',
                text: 'Привет, а о каком видео в чате пишут?',
            },
            {
                sender: 'ВАДИМ',
                text: 'Здаров, звезда!',
            },
            {
                sender: 'ВАДИМ',
                text: 'Я, кароч, чуть подмонтировал видос ',
            },
            {
                sender: 'ВАДИМ',
                text: 'Вот, смотри: https://youtu.be/aWi_HePX1lY?t=247',
                attach: 'video',
            },
            {
                sender: 'ВАДИМ',
                text: 'Ты реально всех порвал',
            },
        ],
    },
    Папа: {
        type: 'private',
        avatar: 'father.png',
        messages: [
            {
                sender: 'Папа',
                text: 'Бабушка звонила. Вы поругались?',
                time: '(20:40)',
                date: '3 Oct',
            },
            {
                sender: 'Папа',
                text: 'Миша, мне жаль, что тебе пришлось переехать в другой город',
            },
            {
                sender: 'Папа',
                text: 'Я обязательно заберу тебя, когда придет время…',
            },
            {
                sender: 'Папа',
                text: 'Постарайся продержаться сынок. Мама бы этого очень хотела…',
            },
            {
                sender: 'Папа',
                text: 'На тебя жаловалась учительница по русскому!',
                time: '(17:52)',
                date: '4 Oct',
            },
            {
                sender: 'Папа',
                text: 'Я уговорил бабушку вернуть телефон',
            },
            {
                sender: 'Папа',
                text: 'позвони срочно!!',
            },
            {
                sender: 'Папа',
                text: 'Нам надо поговорить',
            },
            {
                sender: 'Папа',
                text: 'Мне звонила твоя учительница',
            },
            {
                sender: 'Папа',
                text: 'Пойдем в следующем месяце на эту игру?',
                time: '(17:52)',
                attach: 'album',
                images: ['ticket.png'],
            },
            {
                sender: 'me',
                text: 'Ты знаешь Алису?',
            },
            {
                sender: 'Папа',
                text: 'Я рад, что ты-таки решил мне ответить. Нет, не знаю. Пойдем на Спартак?',
            },
            {
                sender: 'me',
                text: 'ДА!',
            },
            {
                sender: 'Папа',
                text: 'Ты поговорил с бабушкой?',
                time: '(16:24)',
                date: '6 Oct',
            },
            {
                sender: 'Папа',
                text: 'Вам надо помириться',
            },
            {
                sender: 'Папа',
                text: 'Миш?',
            },
            {
                sender: 'me',
                text: 'Поговорил.',
            },
            {
                sender: 'Папа',
                text: 'Ок. Все в порядке?',
            },
            {
                sender: 'me',
                text: 'Да. как дела у мамы?',
            },
            {
                sender: 'Папа',
                text: 'Был сегодня у нее. Передает привет. Состояние пока без изменений',
            },
            {
                sender: 'me',
                text: 'К ней можно?',
            },
            {
                sender: 'Папа',
                text: 'Пока нет',
            },
            {
                sender: 'me',
                text: 'А ты скоро приедешь?',
            },
            {
                sender: 'Папа',
                text: 'Увидим, пока не знаю',
            },
            {
                sender: 'Папа',
                text: 'Миш. Я приеду вечером. Надо поговорить с тобой',
            },
            {
                sender: 'Папа',
                text: 'Я соскучился',
            },
            {
                sender: 'Папа',
                text: 'И не задерживайся слишком долго в школе! Буду ждать!',
            },
            {
                sender: 'me',
                text: 'Прости, сегодня рано прийти не получится.',
            },
            {
                sender: 'me',
                text: 'Я иду с друзьями защищать животных.',
            },
            {
                sender: 'Папа',
                text: 'Ну что? Вы там еще долго?',
                time: '(20:43)',
                date: '6 Oct',
            },
            {
                sender: 'me',
                text: 'Где?',
            },
            {
                sender: 'Папа',
                text: 'Ты же говорил, что ты на собрании по защите животных',
            },
            {
                sender: 'me',
                text: 'Аа. Да, еще час',
            },
            {
                sender: 'me',
                attach: 'album',
                images: ['photo1.jpg'],
            },
            {
                sender: 'Папа',
                text: 'ну, хорошо. Тебя встретить?',
            },
            {
                sender: 'me',
                text: 'Не, пасиб',
            },
        ],
    },
};
