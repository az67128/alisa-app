
export const IS_TEST = /(localhost|192\.168)/.test(
    window.location.href
  );

export const sendMetrik = (name, payload) => {
    const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
    const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
    const dummy = ()=>{}
    const gtag = window.gtag || dummy;
    // const ym = window.ym || consoleMetrik;
  
    ym(74772088, "reachGoal", name, payload);
    
    if(!IS_TEST) {
      gtag('event', name, payload);
    }
  };